TokenScanner.java
Version 2

This project will be about designing a scanner that can read in a string and get tokens from it. 
It will take in a given file and then use that to scan for tokens.
Users will invoke this code using the provided TokenMain.java program.
----------------------
Instructions:
	1. Path your terminal into the TokenScannerPackage directory in this folder.
	2. Run:
		
		java -jar jflex-full-1.7.0.jar TokenScanner.jflex 
		
		This will use the jflex file to build the main scanner class inside of the TokenScannerPackage.
	3. Path to where TokenMain.java is located.
	4. Next Run: 
	
		javac TokenMain.java
		
		This will compile all of the .java files inside of the folder and the package folder.
	5. Finaly run:
	
		java TokenMain YOURTXTFILE.txt
		
	6. The terminal will output what legal and illegal words/symbols were found.
----------------------
Built With:
JFlex - Makes the scanner.
Java - Runs the actual scanner.

----------------------
Author(s):
Nghia Huynh

----------------------
License:
This project is license under nobody and is free to use for all.

----------------------
Acknowledgements:
Google - Telling me what to do.