/**
 * Token.java
 * Compilers 1
 * December 20, 2018
 * This creates a token object given a lexeme and a type.
 * @author Nghia Huynh
 * @version 1
 */

package TokenScannerPackage;

public class Token {
	//The lexeme of the token.
    public String lexeme;
	//The token type.
    public TokenType type;
	//The token constructer.
	public Token( String new_lexeme, TokenType new_type) 
	{
        this.lexeme = new_lexeme;
        this.type = new_type;
    }
}