/**
 * TokenType.java
 * Compilers 1
 * December 20, 2018
 * This holds the enum for all of the different toke types found in our simple pascal.
 * @author Nghia Huynh
 * @version 1
 */

package TokenScannerPackage;

public enum TokenType {
    ID,
	NUMBER,
	INTEGER,
	REAL,
	WHILE,
	DO,
	NOT,
	AND,
	ARRAY,
	BEGIN,
	ELSE,
	END,
	IF,
	OF,
	OR,
	PROGRAM,
	PROCEDURE,
	THEN,
	TYPE,
	VARIABLE,
	EQUAL,
	SEMICOLON,
	COLON,
	ASSIGNOP,
	EQUALCHECK,
	LESS_THAN_EQUAL,
	LESS_THAN,
	GREATER_THAN_EQUAL,
	GREATER_THAN,
	PLUS,
	MINUS,
	MULTIPLY,
	DIVIDE,
	LEFT_BRACKET,
	RIGHT_BRACKET,
	LEFT_PARENTHESIS,
	RIGHT_PARENTHESIS,
	PERIOD,
	COMMA;
}