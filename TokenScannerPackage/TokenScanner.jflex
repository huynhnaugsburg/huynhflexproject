/**
 * TokenScanner.java
 * Compilers 1
 * December 20, 2018
 * This is a scanner that uses regular expression to find tokens in a file.
 * @author Nghia Huynh
 * @version 1
 */

/* Declarations */
package TokenScannerPackage;

import java.util.HashMap;


%%
%class  TokenScanner   /* Names the produced java file */
%public					/*Makes the project file public */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */

%{
	//Creates a lookup table so our scanner can find key words/characters.
    private HashMap<String,TokenType> lookupTable = new HashMap<String,TokenType>();
%}

%init{
		//When the scanner is created this code bellow adds to the lookupTable.
		lookupTable.put("integer", TokenType.INTEGER);
		lookupTable.put("real", TokenType.REAL);
		lookupTable.put("while", TokenType.WHILE);
		lookupTable.put("do", TokenType.DO);
		lookupTable.put("not", TokenType.NOT);
		lookupTable.put("and", TokenType.AND);
		lookupTable.put("array", TokenType.ARRAY);
		lookupTable.put("begin", TokenType.BEGIN);
		lookupTable.put("else", TokenType.ELSE);
		lookupTable.put("end", TokenType.END);
		lookupTable.put("if", TokenType.IF);
		lookupTable.put("of", TokenType.OF);
		lookupTable.put("or", TokenType.OR);
		lookupTable.put("program", TokenType.PROGRAM);
		lookupTable.put("procedure", TokenType.PROCEDURE);
		lookupTable.put("then", TokenType.THEN);
		lookupTable.put("type", TokenType.TYPE);
		lookupTable.put("var", TokenType.VARIABLE);
		lookupTable.put("=", TokenType.EQUAL);
		lookupTable.put(";", TokenType.SEMICOLON);
		lookupTable.put(":", TokenType.COLON);
		lookupTable.put(":=", TokenType.ASSIGNOP);
		lookupTable.put("<>", TokenType.EQUALCHECK);
		lookupTable.put("<=", TokenType.LESS_THAN_EQUAL);
		lookupTable.put("<", TokenType.LESS_THAN);
		lookupTable.put(">=", TokenType.GREATER_THAN_EQUAL);
		lookupTable.put(">", TokenType.GREATER_THAN);
		lookupTable.put("+", TokenType.PLUS);
		lookupTable.put("-", TokenType.MINUS);
		lookupTable.put("*", TokenType.MULTIPLY);
		lookupTable.put("/", TokenType.DIVIDE);
		lookupTable.put("{", TokenType.LEFT_BRACKET);
		lookupTable.put("}", TokenType.RIGHT_BRACKET);
		lookupTable.put("(", TokenType.LEFT_PARENTHESIS);
		lookupTable.put(")", TokenType.RIGHT_PARENTHESIS);
		lookupTable.put(".", TokenType.PERIOD);
		lookupTable.put(",", TokenType.COMMA);
%init}

%eofval{
  return null;
%eofval}
/* Patterns */

other         = .
letter        = [A-Za-z]
word          = {letter}+
digit		  = [0-9]
number		  =	{digit}{digit}*
whitespace    = [ \n\t\r]
relop		  = <=?|<>?|:?=|>=?|:
addop		  = [+|-]
mulop		  = [*|/]
organizer     = [;|\(|\)|\{|\}|\,|\.]

%%
/* Lexical Rules */

{word}     {
             /** Returns the word that was found. */
			 Token answer = null;
			 //This checks for if the word is in the id table, if not make new ID token.
			 if(lookupTable.get(yytext()) == null)
			 {
				answer = new Token(yytext(), TokenType.ID);
			 }
			 //If it is a keyword return the token with correct token value.
			 else
			 {
				answer = new Token(yytext(), lookupTable.get(yytext()));
			 }
			 return answer;
            }
			
{number}	{
				/**Print a  number that was found */
				return(new Token(yytext(),TokenType.NUMBER));
			}
{addop}		{
				/**Return a addop that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}

{mulop}		{
				/**Return a mulop symbol that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}
{relop}		{
				/**Print a relop symbol that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}
{organizer}	{
				/**Print an organizer symbol that was found */
				return(new Token(yytext(), lookupTable.get(yytext())));
			}
{whitespace}
			{
			/**This is ment to ignore white space and new lines*/
			}
			
{other}    { 
             System.out.println("Illegal char: '" + yytext() + "' found.");
           }
		   