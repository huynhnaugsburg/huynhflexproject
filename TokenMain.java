/**
 * TokenMain.java
 * Compilers 1
 * December 20, 2018
 * This is a main which is used to invoke the token scanner, it takes in a file name for its argument.
 * @author Nghia Huynh
 * @version 1
 */
//Imports methods/classes from the TokenScannerPackage.
import TokenScannerPackage.*;
 
public class TokenMain {

    /**
     * @param args Mainly args[0] which is a chosen user file.
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TokenScanner scanner = null;
        //Tries to open a file and if it cannot it will go to one of the catch statements.
		try 
		{
		//This code reads in a given file from the caller.
          java.io.FileInputStream stream = new java.io.FileInputStream(args[0]);
          java.io.Reader reader = new java.io.InputStreamReader(stream, "UTF-8");
          scanner = new TokenScanner(reader);
		  //Checks if the file contains a token.
		  Token new_token = scanner.nextToken();
		  //While the next token is not null then the code will keep scanning for tokens
          while (new_token != null) 
		  {
			  //Print out current found token.
			  System.out.println("Found a Token of " + new_token.type + " type : " + new_token.lexeme);
			  //Checks if there is a token after the current token.
			  new_token = scanner.nextToken();
		  }
        }
		//This catch is for when there is no file found.
		catch (java.io.FileNotFoundException e) 
		{
          System.out.println("File not found : \""+args[0]+"\"");
        }
		//This catch is for when the code cannnot read the file.
		catch (java.io.IOException e) {
          System.out.println("IO error scanning file \""+args[0]+"\"");
          System.out.println(e);
        }
		//This is for when there is a unexspected error.
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
        }
    }
    
}